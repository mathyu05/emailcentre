module.exports =
{
  siteMetadata:
  {
    title: `PRISM Email Centre`,
    description: `PRISM Email Centre`,
    author: `Matthew Baldwin`,
    emails: [
      {
        name: `Request External Counsel`,
        description: `Email Briefings Team to request External Counsel for this hearing`,
        link: `mailto:email@example.com`,
        level: `hearing`
      },
      {
        name: `Contact Legal Admin`,
        description: `Request assistance from the Legal Admin for this Matter`,
        link: `mailto:email@example.com`,
        level: `all`
      },
      {
        name: `Contact Victims and Witnesses`,
        description: `Draft an email to this Matter's Victims and Witnesses`,
        link: `mailto:email@example.com`,
        level: `all`
      },
      {
        name: `Contact Matter VWAS Officer`,
        description: `Desription`,
        link: `mailto:email@example.com`,
        level: `all`
      },
      {
        name: `Send Indictment`,
        description: `Send an Indictment to...`,
        link: `mailto:email@example.com`,
        level: `matter`
      }
    ]
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `PRISM Email Centre`,
        short_name: `Email Centre`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#0e7886`,
        display: `minimal-ui`,
        icon: `src/images/prism-illustration.png`
      }
    },
    `gatsby-plugin-postcss`,
    `gatsby-plugin-offline`
  ]
};