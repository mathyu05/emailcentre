import React from "react";
import prismIllustration from "../images/prism-illustration.svg";
import Search from "../components/search";
import feedbackIllustration from "../images/feedback-illustration.svg";

const Header = (props) =>
{
  return(
    <div>
      <div
        className = "flex bg-white border-b border-gray-200 fixed top-0 inset-x-0 z-100 h-16 items-center"
      >
        <div
          className = "w-full max-w-screen-xl relative mx-auto px-6"
        >
          <div
            className = "flex items-center -mx-6"
          >
            <div
              className = "lg:w-1/3 xl:w-1/4 pl-6 pr-6 lg:pr-8"
            >
              <div
                className = "flex items-center"
              >
                <a
                  href = "/"
                  className = "block lg:mr-4 flex items-center"
                >
                  <img
                    alt = "PRISM"
                    className = "mr-2 h-10 w-auto block"
                    src =
                    {
                      (
                        () =>
                        {
                          return prismIllustration;
                        }
                      )()
                    }
                  />
                  <span className="font-bold text-lg tracking-tight hidden md:block">
                    PRISM Email Centre
                  </span>
                </a>
              </div>
            </div>
            <div
              className = "flex flex-grow lg:w-2/3 xl:w-2/3"
            >
              <div
                className = "w-full lg:px-6 xl:w-3/4 xl:px-12"
              >
                <div
                  className = "relative"
                >
                  <span
                    className = "w-full relative inline-block border-solid border-2 border-gray-200"
                  >
                    <Search />
                  </span>
                </div>
              </div>
              <button
                className = { `flex px-6 items-center lg:hidden text-gray-500 focus:outline-none focus:text-gray-700 ${ props.openMobileNaviation ? "hidden" : "" }` }
                onClick = { () => props.setOpenMobileNavigation(true) }
              >
                <svg className="fillCurrent w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path></svg>
              </button>
              <button
                className = { `flex px-6 items-center lg:hidden text-gray-500 focus:outline-none focus:text-gray-700 ${ props.openMobileNaviation ? "" : "hidden" }` }
                onClick = { () => props.setOpenMobileNavigation(false) }
              >
                <svg className="fillCurrent w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 8.586L2.929 1.515 1.515 2.929 8.586 10l-7.071 7.071 1.414 1.414L10 11.414l7.071 7.071 1.414-1.414L11.414 10l7.071-7.071-1.414-1.414L10 8.586z"></path></svg>
              </button>
              <div
                className = "hidden lg:flex lg:items-center lg:justify-between xl:w-1/4 px-6"
              >
                <div
                  className = "flex items-center"
                >
                  <a
                    href = "/"
                    className = "block lg:mr-4 flex items-center font-medium text-gray-600 hover:text-gray-900"
                  >
                    <img
                      alt = "Feedback"
                      className = "mr-2 h-6 w-auto block"
                      src =
                      {
                        (
                          () =>
                          {
                            return feedbackIllustration;
                          }
                        )()
                      }
                    />
                    <span className="tracking-tight hidden md:block">
                      Give Feedback!
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;