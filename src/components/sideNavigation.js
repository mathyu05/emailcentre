import React from "react";
import allIllustration from "../images/all-illustration.svg";
import matterIllustration from "../images/matter-illustration.svg";
import phaseIllustration from "../images/phase-illustration.svg";
import hearingIllustration from "../images/hearing-illustration.svg";
import SideNavigationOption from "./sideNavigationOption";

const SideNavigation = (props) =>
{
  return(
    <div
      className = { `fixed inset-0 pt-16 h-full bg-white z-90 w-full border-b -mb-16 lg:-mb-0 lg:static lg:h-auto lg:overflow-y-visible lg:border-b-0 lg:pt-0 lg:w-1/4 lg:block lg:border-0 xl:w-1/5 ${ props.openMobileNavigation ? "" : "hidden" }` }
    >
      <div
        className = "h-full overflow-y-auto scrolling-touch lg:h-auto lg:block lg:relative lg:sticky lg:top-16 bg-white lg:bg-transparent"
      >
        <nav
          className = "px-6 pt-6 overflow-y-auto text-base lg:text-sm lg:py-12 lg:pl-6 lg:pr-8 sticky?lg:h-(screen-16)"
        >
          <div
            className = "mb-10"
          >
            <SideNavigationOption
              destination = "/"
              icon = { allIllustration }
              name = "All Emails"
              selected = { props.activePage === "all" }
            />
            <SideNavigationOption
              destination = "/matterEmails"
              icon = { matterIllustration }
              name = "Matter Emails"
              selected = { props.activePage === "matter" }
            />
            <SideNavigationOption
              destination = "/phaseEmails"
              icon = { phaseIllustration }
              name = "Phase Emails"
              selected = { props.activePage === "phase" }
            />
            <SideNavigationOption
              destination = "/hearingEmails"
              icon = { hearingIllustration }
              name = "Hearing Emails"
              selected = { props.activePage === "hearing" }
            />
          </div>
        </nav>
      </div>
    </div>
  );
};

export default SideNavigation;