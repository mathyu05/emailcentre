import React from "react";
import { v4 as UUIDv4 } from "uuid";
import GridElement from "../components/gridElement.js";

const Grid = (props) =>
{
  return (
    <div
      className = "flex flex-wrap"
    >
      {
        props.gridElements.map(
          t => (
            <GridElement
              key = { t.id ?? UUIDv4() }
              title = { t.name }
              description = { t?.description ?? '' }
              addressees = { '' }
              link = { t.link }
            />
          )
        )
      }
    </div>
  );
}

export default Grid;
