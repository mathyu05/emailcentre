import React from "react";

const GridElement = ({ title, description, link }) =>
{
  return (
    <a
      className = "w-full md:w-1/2 lg:w-1/3 h-56 my-2"
      href = { link }
    >
      <div
        className = "h-full m-2 p-2 bg-gray-200 hover:bg-gray-300"
      >
        <p
          className = "text-lg font-semibold"
        >
          { title }
        </p>
        <p
          className = "text-sm font-normal"
        >
          {
            description.length <= 100 ?
              description
              : description.substring(0, 100) + "..."
          }
        </p>
      </div>
    </a>
  );
}

export default GridElement;
