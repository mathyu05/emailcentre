import matchSorter from 'match-sorter';

const getSearchItems = (searchValue, searchItems, { omitSearchItems = [], limit } = {}) =>
{
  const remainingSearchItems = searchItems.filter(
    si => !omitSearchItems.some(osi => osi && osi === si.id),
  );

  const sortedSearchItems = searchValue
    ? matchSorter(
        remainingSearchItems,
      searchValue,
      {
        keys: ['name'],
      }
    )
    : remainingSearchItems;

  const limitedSearchItems = limit
    ? sortedSearchItems.slice(0, limit)
    : sortedSearchItems;

  return limitedSearchItems;
}

const fetchSearchItems = (searchValue, searchItems, { omitSearchItems, limit, requestId } = {}) =>
{
  return new Promise(
    resolve =>
    {
      setTimeout(() =>
      {
        resolve(
          {
            response:
            {
              data: getSearchItems(searchValue, searchItems, {omitSearchItems, limit}),
              requestId,
            },
          }
        );
      }, 0)
    }
  );
}
  
export { fetchSearchItems };