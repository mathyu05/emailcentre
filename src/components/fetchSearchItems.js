import React from "react";
import { fetchSearchItems } from './fetchSearchItemsHelper';
import debounce from 'debounce-fn';

class FetchSearchItems extends React.Component
{
  static initialState =
  {
    loading: false,
    error: null,
    searchItems: []
  };
  requestId = 0;
  state = FetchSearchItems.initialState;
  mounted = false;

  reset(overrides)
  {
    this.setState({ ...FetchSearchItems.initialState, ...overrides });
  }

  fetch = debounce(
    () =>
    {
      if (!this.mounted)
      {
        return;
      }
      const {omitSearchItems, limit} = this.props;
      this.requestId++;
      fetchSearchItems(
        this.props.searchValue,
        this.props.searchItems,
        {
            omitSearchItems,
          limit,
          requestId: this.requestId,
        }
      ).then(
        ({ response: { data: searchItems, requestId } }) =>
        {
          if (this.mounted && requestId === this.requestId)
          {
            this.props.onLoaded({ searchItems });
            this.setState({ loading: false, searchItems });
          }
        },
        ({ response: { error, requestId } }) =>
        {
          if (this.mounted && requestId === this.requestId)
          {
            this.props.onLoaded({ error });
            this.setState({ loading: false, error });
          }
        },
      );
    },
    { wait: 300 },
  );

  prepareFetch()
  {
    this.reset({ loading: true });
  }

  componentDidMount()
  {
    this.mounted = true;
    this.prepareFetch();
    this.fetch();
  }

  componentDidUpdate(prevProps)
  {
    if (prevProps.searchValue !== this.props.searchValue || prevProps.omitSearchItems !== this.props.omitSearchItems)
    {
      this.prepareFetch();
      this.fetch();
    }
  }

  componentWillUnmount()
  {
    this.mounted = false;
  }

  render()
  {
    return this.props.children(this.state);
  }
}

export { FetchSearchItems };