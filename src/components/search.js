import React, { useState } from "react";
import { useStaticQuery, graphql } from 'gatsby';
import Downshift from "downshift";
import { SearchItemList } from './searchItemList';
import { FetchSearchItems } from './fetchSearchItems';

const Search = (props) =>
{
  const { site } = useStaticQuery(
    graphql`
      query
      {
        site
        {
          siteMetadata
          {
            emails
            {
              name
              description
              link
              level
            }
          }
        }
      }
  `);

  const input = React.createRef();
  const [selected, setSelected] = useState([]);
  // conststate =
  // {
  //   selectedContacts: [ 999 ]
  // };

  const handleChange = (selectedMade, downshift) =>
  {
    setSelected(
      [...selected, selectedMade]
    );

    downshift.reset();
  };

  const handleInputKeyDown = (
    {
      event,
      isOpen,
      selectHighlightedItem,
      highlightedIndex,
      reset,
      inputValue,
    }
  ) =>
  {
    if (event.key === 'Backspace' && !event.target.value)
    {
      // remove the last input
      setSelected(
        selected.length ?
          selected.slice(0, selected.length - 1)
          : [],
      );

      reset();
    }
    else if (isOpen && ['Tab', ',', ';'].includes(event.key))
    {
      event.preventDefault();

      if (highlightedIndex != null)
      {
        selectHighlightedItem();
      }
      else
      {
        setSelected(
          ...selected,
          {
            name: inputValue,
            description: inputValue,
            link: inputValue,
            level: inputValue
          }
        );

        reset();
      }
    }
  };


  return (
    <Downshift
      selectedItem = { null }
      onChange = { handleChange }
      defaultHighlightedIndex = { 0 }
    >
      {
        (
          {
            getLabelProps,
            getInputProps,
            getItemProps,
            getMenuProps,
            isOpen,
            toggleMenu,
            clearSelection,
            highlightedIndex,
            selectHighlightedItem,
            setHighlightedIndex,
            reset,
            inputValue,
            clearItems,
            setItemCount,
          }
        ) => (
          <div>
            <div
              className = "flex relative items-center flex-wrap"
            >
              <input
                {
                  ...getInputProps(
                    {
                      id: 'searchInput',
                      ref: input,
                      onKeyDown: event =>
                          handleInputKeyDown(
                          {
                            event,
                            selectHighlightedItem,
                            highlightedIndex,
                            isOpen,
                            reset,
                            inputValue,
                          }
                        )
                      ,
                      placeholder: 'Search Emails',
                      className: "flex-1 rounded py-2 px-3 outline-none w-full min-w-1/4"
                    }
                  )
                }
              />
            </div>
            {
              !isOpen ? null : (
                <ul
                  {
                    ...getMenuProps(
                      {
                        className: "p-0 m-0"
                      }
                    )
                  }
                >
                  <FetchSearchItems
                    searchValue = { inputValue }
                    searchItems = { site.siteMetadata.emails }
                    omitSearchItems = { selected }
                    onLoaded =
                    {
                      ({ searchItems }) =>
                      {
                        clearItems();
                        if (searchItems)
                        {
                          setHighlightedIndex(searchItems.length ? 0 : null);
                          setItemCount(searchItems.length);
                        }
                      }
                    }
                  >
                    {
                      ({ loading, searchItems, error }) => (
                        <div
                          className = "absolute bg-white max-h-1/2 shadow border-solid border-1 mt-1 border-gray-600"
                        >
                          {
                            loading ? (
                              <div
                                className = "p-4 text-center"
                                style =
                                {
                                  {
                                    width: document.getElementById('searchInput').offsetWidth
                                  }
                                }
                              >
                                loading...
                              </div>
                            ) : error ? (
                              <div
                                className = "p-4 text-center"
                                style =
                                {
                                  {
                                    width: document.getElementById('searchInput').offsetWidth
                                  }
                                }
                              >
                                error...
                              </div>
                            ) : searchItems.length ? (
                              <SearchItemList
                                highlightedIndex = { highlightedIndex }
                                getItemProps = { getItemProps }
                                searchItems = { searchItems }
                              />
                            ) : (
                              <div
                                className = "p-4 text-center"
                                style =
                                {
                                  {
                                    width: document.getElementById('searchInput').offsetWidth
                                  }
                                }
                              >
                                no results...
                              </div>
                            )
                          }
                        </div>
                      )
                    }
                  </FetchSearchItems>
                </ul>
              )
            }
          </div>
        )
      }
    </Downshift>
  );
}

export default Search;
