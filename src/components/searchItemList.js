import React from "react";
import { List } from 'react-virtualized';
import { Link } from "gatsby";
import allIllustration from "../images/all-illustration.svg";
import matterIllustration from "../images/matter-illustration.svg";
import phaseIllustration from "../images/phase-illustration.svg";
import hearingIllustration from "../images/hearing-illustration.svg";

const SearchItemList = ({ highlightedIndex, getItemProps, searchItems }) =>
{
  const rowHeight = 50;
  const fullHeight = searchItems.length * rowHeight;

  const rowRenderer = ({ key, index, style }) => (
    <li
      key = { searchItems[index].id }
      {
        ...getItemProps(
          {
            item: searchItems[index],
            index,
            style,
            className: `absolute top-0 w-full h-12 left-0 cursor-pointer px-4 ${ highlightedIndex === index ? 'bg-gray-400' : 'bg-white' }`
          }
        )
      }
    >
      <Link
        to = { searchItems[index].link }
      >
        <div
          className = "flex justify-center"
        >
          <img
            alt = "Emails"
            className = "flex-none h-12 py-2 mr-2"
            src =
            {
              (
                () =>
                {
                  switch (searchItems[index].level)
                  {
                    case "matter":
                      return matterIllustration;
                    case "phase":
                      return phaseIllustration;
                    case "hearing":
                      return hearingIllustration;
                    default:
                      return allIllustration;
                  }
                }
              )()
            }
          />
          <div
            className = "flex-1"
          >
            <div>
              { searchItems[index].name }
            </div>
            <div
              className = "text-sm	ml-1"
            >
              { searchItems[index].description }
            </div>
          </div>
        </div>
      </Link>
    </li>
  );

  return (
    <List
      width = { document.getElementById('searchInput').offsetWidth }
      scrollToIndex = { highlightedIndex || 0 }
      height = { fullHeight > 280 ? 280 : fullHeight }
      rowCount = { searchItems.length }
      rowHeight = { rowHeight }
      rowRenderer = { ({ key, index, style }) => rowRenderer({ key, index, style }) }
    />
  )
};

export { SearchItemList }