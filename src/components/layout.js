import React, { useState } from "react";
import Header from "./header";
import Footer from "./footer";
import SideNavigation from "./sideNavigation";

const Layout = (props) =>
{
  const [openMobileNaviation, setOpenMobileNavigation] = useState(false);

  return(
    <div>
      <Header
        openMobileNaviation = { openMobileNaviation }
        setOpenMobileNavigation = { open => setOpenMobileNavigation(open) }
      />
      <div
        className = "w-full max-w-screen-xl mx-auto px-6"
      >
        <div
          className = "lg:flex -mx-6"
        >
          <SideNavigation
            activePage = { props.activePage }
            openMobileNavigation = { openMobileNaviation }
          />
          <div
            className = "min-h-screen w-full lg:static lg:max-h-full lg:overflow-visible lg:w-3/4 xl:w-4/5"
          >
            <div>
              <div
                className = "flex"
              >
                { props.children }
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Layout;