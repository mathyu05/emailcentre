import React from "react";
import { useStaticQuery, graphql } from "gatsby";
import Grid from "../components/grid";

const ContentBody = (props) =>
{
  const { site } = useStaticQuery(
    graphql`
      query
      {
        site
        {
          siteMetadata
          {
            emails
            {
              name
              description
              link
              level
            }
          }
        }
      }
  `);

  return(
    <div
      className = "flex"
    >
      <div
        className = "text-opacity-100 text-gray-700 leading-relaxed px-6 xl:px-12 w-full max-w-3xl mx-auto lg:ml-0 lg:mr-auto xl:mx-0 xl:w-3/4"
      >
        <Grid
          gridElements = { props.emailFilter(site.siteMetadata.emails) }
        />
      </div>
    </div>
  );
};

export default ContentBody;