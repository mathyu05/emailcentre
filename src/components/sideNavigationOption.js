import React from "react";
import { Link } from "gatsby";

const SideNavigationOption = (props) =>
{
  return(
    <Link
      to = { props.destination }
      className = { `flex items-center px-2 -mx-2 py-1 font-medium ${ props.selected ? "bg-teal-200 bg-opacity-25" : "" } text-gray-600 ${ props.selected ? "" : "hover:text-gray-900" }` }
    >
      <img
        alt = { props.name }
        className = "h-6 w-6"
        src = { props.icon }
      />
      <span
        className = "ml-3"
      >
        { props.name }
      </span>
    </Link>
  );
};

export default SideNavigationOption;