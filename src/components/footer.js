import React from "react";

const Footer = (props) =>
{
  return(
    <div>
      <div
        className = "flex bg-white border-b border-gray-200 fixed bottom-0 inset-x-0 z-100 h-16 items-center ml-4"
      >
        <div
          className = "text-right text-gray-300"
        >
          Icons made by
          {' '}
          <a
            href = "https://www.flaticon.com/authors/freepik"
            title = "Freepik"
            className = "font-medium"
          >
            Freepik
          </a>
          {' '}
          from
          {' '}
          <a
            href = "https://www.flaticon.com/"
            title = "Flaticon"
            className = "font-medium"
          >
            www.flaticon.com
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;