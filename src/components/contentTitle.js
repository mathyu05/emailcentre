import React from "react";

const ContentTitle = (props) =>
{
  return(
    <div
      className = "text-opacity-100 text-gray-700 leading-relaxed mb-6 px-6 max-w-3xl mx-auto lg:ml-0 lg:mr-auto xl:mx-0 xl:px-12 xl:w-3/4"
    >
      <h1
        className = "flex items-center mb-1 leading-none text-opacity-100 text-gray-900 font-medium text-3xl tracking-tight"
      >
        { props.title }
      </h1>
      <div
        className = "mt-0 mb-4 text-gray-600"
      >
        { props.subtitle }
      </div>
    </div>
  );
};

export default ContentTitle;