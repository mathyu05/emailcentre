import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import ContentTitle from "../components/contentTitle";
import ContentBody from "../components/contentBody";

const AllEmails = () =>
{
  return (
    <Layout
      activePage = "all"
    >
      <SEO
        keywords = { [`PRISM`, `Email`, `Centre`, `Home`, `All`, `Emails`] }
        title = "Home"
      />
      <div
        className = "pt-24 pb-16 lg:pt-28 w-full"
      >
        <ContentTitle
          title = "All Emails"
          subtitle = "All Emails in PRISM"
        />
        <ContentBody
          emailFilter = { emails => emails }
        />
      </div>
    </Layout>
  );
};

export default AllEmails;