import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import ContentTitle from "../components/contentTitle";
import ContentBody from "../components/contentBody";

const HearingEmails = () =>
{
  return (
    <Layout
      activePage = "hearing"
    >
      <SEO
        keywords = { [`PRISM`, `Email`, `Centre`, `Home`, `All`, `Emails`] }
        title = "Home"
      />
      <div
        className = "pt-24 pb-16 lg:pt-28 w-full"
      >
        <ContentTitle
          title = "Hearing Emails"
          subtitle = "Emails specific to Hearings in PRISM"
        />
        <ContentBody
          emailFilter = { emails => emails.filter(email => email.level === 'all' || email.level === "hearing") }
        />
      </div>
    </Layout>
  );
};

export default HearingEmails;