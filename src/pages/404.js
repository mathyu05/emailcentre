import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import ContentTitle from "../components/contentTitle";

const NotFoundPage = (props) =>
{
  return (
    <Layout>
      <SEO title = "404: Not found" />
      <div
        className = "pt-24 pb-16 lg:pt-28 w-full"
      >
        <ContentTitle
          title = "Page Not Found"
          subtitle = "Please turn around when safe"
        />
        <div
          className = "flex"
        >
          <div
            className = "text-opacity-100 text-gray-700 leading-relaxed px-6 xl:px-12 w-full max-w-3xl mx-auto lg:ml-0 lg:mr-auto xl:mx-0 xl:w-3/4"
          >
          </div>
        </div>
      </div>
    </Layout>

  );
};

export default NotFoundPage;