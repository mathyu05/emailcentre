import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import ContentTitle from "../components/contentTitle";
import ContentBody from "../components/contentBody";

const PhaseEmails = () =>
{
  return (
    <Layout
      activePage = "phase"
    >
      <SEO
        keywords = { [`PRISM`, `Email`, `Centre`, `Home`, `All`, `Emails`] }
        title = "Home"
      />
      <div
        className = "pt-24 pb-16 lg:pt-28 w-full"
      >
        <ContentTitle
          title = "Phase Emails"
          subtitle = "Emails specific to Phases in PRISM"
        />
        <ContentBody
          emailFilter = { emails => emails.filter(email => email.level === 'all' || email.level === "phase") }
        />
      </div>
    </Layout>
  );
};

export default PhaseEmails;